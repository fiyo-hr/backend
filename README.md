# Backend test

Welcome at Fiyo. This test is created to get a general overview of your backend capabilities. Please spend a maximum of one hour to this test.

Create a merge request to this repository when finished.

## Goal
In the next hour you’re going to create a calculator. It must be possible to execute the calculator via the CLI. You're free to use any language or tool (_as long as you write the calculator yourself_).

**Example run scripts**

_If you choose PHP_
```
php calculate.php 3+2
```

_If you choose typescript_
```
node calculate.ts 3+2
```

**Example calculations**

| Calculation | Outcome |
| ------ | ------ |
| 3+5 | 8 |
| 4x5 | 20 |
| 10/5 | 2 |
| 10-5 | 5 |
| 3+5-2 | 6 |
| 4x5+1 | 21 |
| 10/5x2 | 4 |
| 1+2x3 | 7 |
| (1+2)x3) | 9 |
| 2^10 | 1024 |

**Bonus points**
Setup a gitlab pipe which tests your code.
